import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Scanner;

public class SendTweetConsole {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter Consumer Key:");
        String consumerKey = in.nextLine();

        System.out.println("Enter Consumer Secret:");
        String consumerSecret = in.nextLine();

        System.out.println("Enter Acces Token:");
        String accesToken = in.nextLine();

        System.out.println("Enter Acces Token Secret:");
        String accesTokenSecret = in.nextLine();

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(accesToken)
                .setOAuthAccessTokenSecret(accesTokenSecret);

        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();

        System.out.println("Enter new tweet:");
        String newTweet = in.nextLine();

        try {
            twitter.updateStatus(newTweet);
        } catch (TwitterException e) {
            System.out.println("Error message: " + e.getErrorMessage());
            System.out.println("HTTP status code: " + e.getStatusCode());
        }
    }
}