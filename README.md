# README. Send Tweet Console #

#1. Описание
Для того что бы отправить новый твит, необходимо следовать инструкциям в программе. 

Все данные (Consumer Key, Consumer Secret, ..., в том числе текст твита) нужно вводить строго в одной строке.

В случае возникновения каких-либо ошибок в процессе отправление, программа сообщит об этом и выведет сообщение и код ошибки.

#2. Инструкция по сборке и запуску
$ git clone https://sievmi@bitbucket.org/sievmi/sendtweetconsole.git

$ cd sendtweetconsole/

$ mvn clean package 

$ mvn exec:java